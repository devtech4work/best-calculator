import React, { Component } from "react";
import "./script.css";
import { Button } from "./components/Button";
import { Input } from "./components/Input";
import { ClearButton } from "./components/ClearButton";
import * as math from "mathjs";

class Script extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
      isDegree: false,
      newval: "",
    };
  }

  addToInput = (val) => {
    this.setState({
      input: this.state.input + val,
    });
  };

  handleEqual = (val) => {
    this.setState({
      input: math.eval(this.state.input),
    });
  };
  radian = (val) => {
    this.setState({ isDegree: false });
  };
  degree = (val) => {
    this.setState({ isDegree: true });
  };
  handleSin = (val) => {
    this.setState({ input: this.state.input + val });
  };

  handleCos = (val) => {
    this.setState({
      input: this.state.input + val,
    });
  };
  handleTan = (val) => {
    this.setState({
      input: this.state.input + val,
    });
  };
  handleAsin = (val) => {
    this.setState({
      input: this.state.input + val.replace("resin", "asin"),
    });
  };

  handleDegreeSin = (val) => {
    this.setState({
      input: math.sin(this.state.input * (math.PI / 180)),
    });
  };
  handleDegreeCos = (val) => {
    this.setState({
      input: math.cos(this.state.input * (math.PI / 180)),
    });
  };
  handleDegreeTan = (val) => {
    this.setState({
      input: math.tan(this.state.input * (math.PI / 180)),
    });
  };
  handleDegreeAsin = (val) => {
    this.setState({
      input: math.asin(this.state.input * (math.PI / 180)),
    });
  };
  handlePi = (val) => {
    this.setState({
      input: this.state.input + val,
    });
  };
  handleMultiply = (val) => {
    if (val === "x") {
      this.setState({
        input: this.state.input + val.replace("x", "*"),
      });
    }
  };
  bracketLeft = (val) => {
    this.setState({
      input: this.state.input + val,
    });
  };
  bracketRight = (val) => {
    this.setState({
      input: this.state.input + val,
    });
  };
  render() {
    const isDegree = this.state.isDegree;
    return (
      <div className="app">
        <div className="calculator-wrapper">
          <Input input={this.state.input}></Input>
          {isDegree && (
            <div className="row">
              <Button handleClick={this.radian}>rad</Button>
              <Button handleClick={this.handleDegreeSin}>sin</Button>
              <Button handleClick={this.handleDegreeCos}>cos</Button>
              <Button handleClick={this.handleDegreeTan}>tan</Button>
              <Button handleClick={this.handleDegreeAsin}>resin</Button>
            </div>
          )}

          {!isDegree && (
            <div className="row">
              <Button handleClick={this.degree}>deg</Button>
              <Button handleClick={this.handleSin}>sin</Button>
              <Button handleClick={this.handleCos}>cos</Button>
              <Button handleClick={this.handleTan}>tan</Button>
              <Button handleClick={this.handleAsin}>resin</Button>
            </div>
          )}
          <div className="row">
            <Button handleClick={this.addToInput}>7</Button>
            <Button handleClick={this.addToInput}>8</Button>
            <Button handleClick={this.addToInput}>9</Button>
            <Button handleClick={this.bracketLeft}>(</Button>
            <Button handleClick={this.bracketRight}>)</Button>
          </div>

          <div className="row">
            <Button handleClick={this.addToInput}>4</Button>
            <Button handleClick={this.addToInput}>5</Button>
            <Button handleClick={this.addToInput}>6</Button>
            <Button handleClick={this.handleMultiply}>x</Button>
            <Button handleClick={this.addToInput}>-</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>1</Button>
            <Button handleClick={this.addToInput}>2</Button>
            <Button handleClick={this.addToInput}>3</Button>
            <Button handleClick={this.addToInput}>+</Button>
            <Button handleClick={this.handlePi}>pi</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>0</Button>
            <Button handleClick={this.addToInput}>.</Button>
            <Button handleClick={() => this.handleEqual()}>=</Button>
            <Button handleClick={this.addToInput}>/</Button>
          </div>
          <div className="row">
            <ClearButton handleClear={() => this.setState({ input: "" })}>
              clear
            </ClearButton>
          </div>
        </div>
      </div>
    );
  }
}

export default Script;
